import parameter
from config import *
from domain_extension import start_index_f_raw
from features_extension import *


def generate_f_raw(data, feature=F_Raw):
    raw_length = 5 if feature == F_Raw_5 else parameter.Param_Pattern_Length
    for event_id, event in data['events'].iteritems():
        time_series = event['time_series'] if feature != F_Raw_Norm else event['time_series_norm']
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_raw(event, f_p(feature), i, time_series, raw_length)

        for i in event['n_idx']:
            build_raw(event, f_n(feature), i, time_series, raw_length)

        add_generated_feature(data, feature)
    return data


def build_raw(event, field, idx, time_series, raw_length):
    event[field].append(time_series[start_index_f_raw(idx, raw_length): idx + 1])



