import math
import random
from math import ceil

import numpy as np

import parameter
from config import PatternsProfile_Default, P_MinMax, P_RandomNeg, P_AllNeg, F_Raw, F_Raw_Dict, F_Airl_Dict, F_Paa, \
    F_Line, F_Word, F_Sentiment, F_D_Spike, F_N_Local_Spike, F_Latest_Spike, F_User, P_HotRange, T_Scaled, \
    P_EmergingScore
from data_access import *
from data_access import get_data_full, write_patterns_features
from domain_extension import get_time_slot_idx, get_time_slot_hours_idx, start_index_f_raw, get_hot_range, \
    pattern_rev_end_range
from features_btw_posts import generate_f_paa, generate_f_line, generate_f_word, generate_f_sentiment
from features_fluctuation import generate_f_fluctuation_d_spike, generate_f_fluctuation_n_local_spike, \
    generate_f_fluctuation_latest_spike
from features_raw import generate_f_raw
from features_timeseries import generate_f_timeseries_airl
from features_user import generate_f_user
from parameter import Param_Scaled_All_Slot_Num, \
     Time_Slot_D, EWMA_Alpha


def generate_p_n_idx(data):
    events = data['events']
    for event_id, event in events.iteritems():
        arg_max = event['arg_max_time_series']
        event['p_idx'] = []
        event['n_idx'] = []
        # arg_max
        #if Param_Pattern_Length - 1 <= arg_max - Predict_For:
        event['p_idx'] = [arg_max - parameter.Predict_For]

        # arg min
        n_idx = find_satisfied_min(event)
        if n_idx > 0:
            event['n_idx'] = [n_idx - parameter.Predict_For]
    return data


def generate_p_n_by_emerging_score(data):
    events = data['events']
    for event_id, event in events.iteritems():
        event['p_idx'] = []
        event['n_idx'] = []
        for idx in range(parameter.Param_Pattern_Length + parameter.Predict_For -1, event['time_slot_num']):
            if emerging_score(event['time_series_norm'][start_index_f_raw(idx):idx + 1]) >= 0.04167015444:
                event['p_idx'].append(idx - parameter.Predict_For)
            else:
                if random.randint(1, 100) < 4:
                    event['n_idx'].append(idx - parameter.Predict_For)
    return data


def generate_p_n_by_post_range(data):
    events = data['events']
    for event_id, event in events.iteritems():
        event['p_idx'] = []
        event['n_idx'] = []
        for idx in range(0, event['time_slot_num']):
            posts_in_hot_range = sum(event['time_series'][get_hot_range(idx)[0]:get_hot_range(idx)[1]])
            if parameter.Param_Pattern_Length - 1 <= idx - parameter.Predict_For:
                if posts_in_hot_range >= 14:
                    event['p_idx'].append(idx - parameter.Predict_For)
                else:
                    event['n_idx'].append(idx - parameter.Predict_For)
    return data


def find_hot_range_posts(data):
    events = data['events']
    avg_list = []
    for event_id, event in events.iteritems():
        arg_max = event['arg_max_time_series']
        hot_posts = sum(event['time_series'][get_hot_range(arg_max)[0]:get_hot_range(arg_max)[1]])
        print hot_posts
        avg_list.append(hot_posts)
    avg_hot_post = sum(avg_list) / len(avg_list)
    print 'avg:', avg_hot_post
    return avg_hot_post


def find_hot_range_by_ewma(data):
    events = data['events']
    for event_id, event in events.iteritems():
        arg_max = event['arg_max_time_series']
        # emerging_scores_event = []
        # for i in range(get_hot_range(arg_max)[0], get_hot_range(arg_max)[1]):
        #    if i + 1 >= Param_Pattern_Length:
        #        emerging_scores_event.append(emerging_score(event['time_series'][start_index_f_raw(i):i]))
        # print 'event: '+event_id
        # print event['time_series'][start_index_f_raw(get_hot_range(arg_max)[0]):get_hot_range(arg_max)[1]]
        # print ",".join([str(i) for i in emerging_scores_event])
        if arg_max + 1 >= parameter.Param_Pattern_Length:
             a = emerging_score(event['time_series_norm'][start_index_f_raw(arg_max):arg_max+1])
             print a
             #if (a <= 0):
             #    print a
             #    print arg_max, " ", start_index_f_raw(arg_max)
             #    print event['time_series']
             #    print event['time_series'][start_index_f_raw(arg_max):arg_max+1]
        #if arg_max + parameter.Param_Pattern_Length <= event['time_slot_num']:
        #    print emerging_score_rev(event['time_series_norm'][arg_max:pattern_rev_end_range(arg_max)])


def generate_p_random_n_idx(data):
    events = data['events']
    for event_id, event in events.iteritems():
        arg_max = event['arg_max_time_series']
        event['p_idx'] = []
        event['n_idx'] = []
        # arg_max
        if parameter.Param_Pattern_Length - 1 <= arg_max - 1:
            event['p_idx'] = [arg_max - 1]

        # random not in arg max
        negative_idx_1 = -1
        if parameter.Param_Pattern_Length - 1 <= arg_max - parameter.Param_Pattern_Length_2:
            negative_idx_1 = random.randint(parameter.Param_Pattern_Length - 1,
                                            arg_max - parameter.Param_Pattern_Length_2)
        negative_idx_2 = -1
        if arg_max + parameter.Param_Pattern_Length_2 <= event['time_slot_num'] - 1:
            negative_idx_2 = random.randint(max(parameter.Param_Pattern_Length - 1, arg_max
                                                + parameter.Param_Pattern_Length_2),
                                            event['time_slot_num'] - 1)
        if negative_idx_1 != -1:
            event['n_idx'] = [negative_idx_1]
        else:
            if negative_idx_2 != -1:
                event['n_idx'] = [negative_idx_2]
    return data


def generate_p_all_n_idx(data):
    events = data['events']
    for event_id, event in events.iteritems():
        arg_max = event['arg_max_time_series']
        event['p_idx'] = []
        event['n_idx'] = []
        # arg_max
        if parameter.Param_Pattern_Length - 1 <= arg_max - 1:
            event['p_idx'] = [arg_max - 1]

        # random not in arg max
        if parameter.Param_Pattern_Length - 1 <= arg_max - parameter.Param_Pattern_Length_2:
            event['n_idx'].extend(range(parameter.Param_Pattern_Length - 1, arg_max - parameter.Param_Pattern_Length_2 + 1))

        if arg_max + parameter.Param_Pattern_Length_2 <= event['time_slot_num'] - 1:
            event['n_idx'].extend(range(max(parameter.Param_Pattern_Length - 1, arg_max + parameter.Param_Pattern_Length_2), event['time_slot_num'] - 1 + 1))

    return data


def find_satisfied_min(event):
    arg_min = -1
    curr_min = 999999999
    for index, time_slot in enumerate(event['time_series']):
        arg_max = event['arg_max_time_series']
        if parameter.Param_Pattern_Length - 1 <= index - parameter.Predict_For and time_slot < curr_min and \
                abs(arg_max - index) >= parameter.Param_Pattern_Length_2:
            arg_min = index
            curr_min = time_slot
    return arg_min


def map_to_time_series(data, time_slot_config=T_Scaled, date_field='CreatedAt'):
    events = data['events']

    for event_id, event in events.iteritems():
        time_slot_num = Param_Scaled_All_Slot_Num if time_slot_config == T_Scaled else \
            int(ceil(float(event['date_range'].total_seconds()) / Time_Slot_D))
        event['time_slot_num'] = time_slot_num

        event['time_series'] = [0 for _ in range(time_slot_num)]

        for tweet in event['list']:
            time_slot_idx = get_time_slot_idx(tweet, event, time_slot_num, date_field)
            tweet['time_slot_idx'] = time_slot_idx
            event['time_series'][time_slot_idx] += 1

        event['time_series_norm'] = [float(t) / float(event['num_tweets']) for t in event['time_series']]

        event['arg_min_time_series'] = int(np.argmin(event['time_series']))
        start_index = parameter.Param_Pattern_Length + parameter.Predict_For -1;
        event['arg_max_time_series'] = int(np.argmax(event['time_series'][start_index:]) + start_index)
    return data


def generate_patterns_features(profile=PatternsProfile_Default):
    for time_slot_config in profile.time_slot_configs:
        for pattern in profile.patterns_generation:
            for u in profile.u_params:
                for k in profile.k_params:
                    parameter.Param_Pattern_Length = k
                    parameter.Param_Pattern_Length_2 = parameter.Param_Pattern_Length / 2
                    parameter.Predict_For = u

                    data = map_to_time_series(get_data_full(), time_slot_config)

                    data_with_patterns = generate_patterns(data, pattern)
                    data_with_patterns_features = generate_features(data_with_patterns, profile.generate_features)
                    write_patterns_features(data_with_patterns_features, pattern, time_slot_config, k, u)


def generate_patterns(data, patterns_generation=P_MinMax):
    data_with_patterns = data
    if patterns_generation == P_RandomNeg:
        generate_p_random_n_idx(data)
    if patterns_generation == P_AllNeg:
        data_with_patterns = generate_p_all_n_idx(data)
    if patterns_generation == P_MinMax:
        data_with_patterns = generate_p_n_idx(data)
    if patterns_generation == P_HotRange:
        data_with_patterns = generate_p_n_by_post_range(data)
    if patterns_generation == P_EmergingScore:
        data_with_patterns = generate_p_n_by_emerging_score(data)
    return data_with_patterns


def generate_features(data, features=[F_Raw]):
    data_with_features = data
    for feature in features:
        if feature in F_Raw_Dict:
            data_with_features = generate_f_raw(data_with_features, feature)
        if feature in F_Airl_Dict:
            data_with_features = generate_f_timeseries_airl(data_with_features, feature)
        if feature == F_Paa:
            data_with_features = generate_f_paa(data_with_features, feature)
        if feature == F_Line:
            data_with_features = generate_f_line(data_with_features, feature)
        if feature == F_Word:
            data_with_features = generate_f_word(data_with_features, feature)
        if feature == F_Sentiment:
            data_with_features = generate_f_sentiment(data_with_features, feature)
        if feature == F_D_Spike:
            data_with_features = generate_f_fluctuation_d_spike(data_with_features)
        if feature == F_N_Local_Spike:
            data_with_features = generate_f_fluctuation_n_local_spike(data_with_features)
        if feature == F_Latest_Spike:
            data_with_features = generate_f_fluctuation_latest_spike(data_with_features)
        if feature == F_User:
            data_with_features = generate_f_user(data_with_features)
    return data_with_features


def ewma(new, old, alpha=EWMA_Alpha):
    return new * alpha + old * (1 - alpha)


def ewma_list(list_data, start_index=0, end_index=-1):
    if end_index == -1:
        end_index = len(list_data) - 1
    result = list_data[start_index]
    for i in range(start_index+1, end_index + 1):
        result = ewma(list_data[i], result)
    return result


def ewma_var(list_data, start_index=0, end_index=-1, alpha=EWMA_Alpha):
    if end_index == -1:
        end_index = len(list_data) - 1
    mean = list_data[start_index]
    variance = 0
    for i in range(start_index+1, end_index + 1):
        diff = list_data[i] - mean
        increase = alpha*diff
        mean = mean + increase
        variance = (1-alpha)*(variance + diff*increase)
    return variance


def ewma_list_rev(list_data, start_index=-1, end_index=0):
    if start_index == -1:
        start_index = len(list_data) - 1
    result = list_data[start_index]
    for i in range(start_index-1, end_index - 1, -1):
        result = ewma(list_data[i], result)
    return result


def ewma_var_rev(list_data, start_index=-1, end_index=0, alpha=EWMA_Alpha):
    if start_index == -1:
        start_index = len(list_data) - 1
    mean = ewma_list_rev[start_index]
    variance = 0
    for i in range(start_index-1, end_index - 1, -1):
        diff = list_data[i] - mean
        increase = alpha*diff
        mean = mean + increase
        variance = (1-alpha)*(variance + diff*increase)
    return variance


def emerging_score(list_data):
    return (list_data[-1] - ewma_list(list_data[0:-1])) / (1 + math.sqrt(ewma_var(list_data[0:-1])))


def emerging_score_rev(list_data):
    return (list_data[0] - ewma_list_rev(list_data[1:])) / (1 + math.sqrt(ewma_var(list_data[1:])))

