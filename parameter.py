# all
Param_Scaled_All_Slot_Num = 100
Param_Pattern_Length = 10
Param_Pattern_Length_2 = Param_Pattern_Length / 2

Minimum_Tweet = 10
Predict_For = 1

# new pattern generation
TweetHotRange = 5
TweetHotRangePosts = 14

# features
Param_F_Spike_Order = 1
Param_F_Ppa = 5

# Tweet timeslot
Time_Slot_D = 300

# EWMA
EWMA_Alpha = 0.8

