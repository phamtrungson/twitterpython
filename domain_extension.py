import parameter


def get_tweets_by_time_slot_idx(event, time_slot_idx):
    return [tweet for tweet in event['list'] if time_slot_idx == tweet['time_slot_idx']]


def get_tweets_by_pattern_idx(event, time_slot_idx):
    return [tweet for tweet in event['list'] if is_in_pattern(tweet['time_slot_idx'], time_slot_idx)]


def get_user(tweet):
    return tweet['TweetDTO']['user']


def get_full_text(tweet):
    return tweet['TweetDTO']['full_text']


def is_in_pattern(checking_idx, idx):
    return 0 <= (idx - checking_idx) <= (parameter.Param_Pattern_Length - 1)


def get_time_slot_idx(tweet, event, time_slot_num, date_field='CreatedAt'):
    range_second = float(event['date_range'].total_seconds())
    return 0 if range_second <= 0 \
        else int(round((tweet[date_field] - event['min_date']).total_seconds() / range_second * (time_slot_num - 1)))


def get_time_slot_hours_idx(tweet, event, date_field='CreatedAt'):
    return int(round((tweet[date_field] - event['min_date']).total_seconds() / parameter.Time_Slot_D))


def get_hot_range(idx):
    return max(0, idx - parameter.TweetHotRange), min(99, idx + parameter.TweetHotRange + 1)


def start_pattern_index(end_index):
    return end_index - parameter.Param_Pattern_Length + 1


def pattern_rev_end_range(end_index):
    return end_index + parameter.Param_Pattern_Length


def start_index_f_raw(end_index, f_raw_length_param = -1):
    f_raw_length = parameter.Param_Pattern_Length if f_raw_length_param == -1 else parameter.Param_Pattern_Length
    return end_index - f_raw_length + 1


def start_index_f_timeseries_l(end_index, param_l):
    return end_index - (parameter.Param_Pattern_Length - param_l + 1) + 1


def start_index_f_timeseries_each_l(end_index, param_l):
    return end_index - param_l + 1

