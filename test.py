import csv
import numpy as np
from sklearn import svm
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import cross_val_score

X = []
Y = []
with open('test_data/iris.data.csv', 'rb') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
	for row in spamreader:
		# print ','.join(row)
		# x.append(map(lambda x : x, row[:-1]))
		if len(row) > 0:
			mappeddata = map(float, row[:-1])
			X.append(mappeddata)
			Y.append(row[4])

clf = svm.SVC(gamma='scale')
clf.fit(X, Y)
print cross_val_score(clf, X, Y, scoring='f1_micro', cv=4)  

# sel = VarianceThreshold(threshold= 0.9)
# X_var_selection = sel.fit_transform(X)
# print len(X[0])
# print len(X_var_selection[0])

# print X_var_selection

# print clf.predict([[5.0, 3.5, 1.6, 0.6]])  



# Xn = np.array(X)
# print Xn
# with open('test_data/iris.output.csv', 'wb') as csvWriteFile:
	# spamwriter = csv.writer(csvWriteFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
	# spamwriter.writerow(Xn[[0]])
	# spamwriter.writerow(Xn[[1]])


X = []
Y = []
i = 0;
with open('test_data/event2012-general-filter1-nega3.csv', 'rb') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
	for row in spamreader:
		# print ','.join(row)
		# X.append(map(lambda x : x, row[:-1]))
		if len(row) > 0 and i != 0:
			nprow = np.array(row)
			mappedData = map(float, nprow[1:20])
			X.append(mappedData)
			Y.append(nprow[21])
		i += 1

clf = svm.SVC(gamma='scale')
clf.fit(X, Y)
print cross_val_score(clf, X, Y, scoring='f1_macro', cv=5)

X = []
Y = []
i = 0;
with open('test_data/event2012-timeseri-filter3.csv', 'rb') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
	for row in spamreader:
		# print ','.join(row)
		# X.append(map(lambda x : x, row[:-1]))
		if len(row) > 0 and i != 0:
			nprow = np.array(row)
			# mappedData = map(float, nprow[1:20])
			X.append([nprow[1]])
			Y.append(nprow[2])
		i += 1

clf = svm.SVC(gamma='scale')
clf.fit(X, Y)
print cross_val_score(clf, X, Y, scoring='f1_micro', cv=5)  

