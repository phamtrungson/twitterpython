# Raw
from profile import PatternsProfile, LearningProfile

F_Raw = 'f_raw'
F_Raw_Norm = 'f_raw_norm'
F_Raw_5 = 'f_raw_5'
F_Raw_Dict = {
    F_Raw: F_Raw,
    F_Raw_Norm: F_Raw_Norm,
    F_Raw_5: F_Raw_5
}

# Fluctuation
F_N_Local_Spike = 'f_n_local_spike'
F_D_Spike = 'f_d_spike'
F_Latest_Spike = 'f_latest_spike'

# User
F_User = 'f_user'

# Time series
F_Airl = 'f_airl'
F_Airl_5 = 'f_airl_5'
F_Aarl = 'f_aarl'
F_Aarl_5 = 'F_aarl_5'
F_Airl_Dict = {
    F_Airl: F_Airl,
    F_Aarl: F_Aarl,
    F_Airl_5: F_Airl_5,
    F_Aarl_5: F_Aarl_5
}
F_Paa = 'F_paa'

F_Line = 'F_line'
F_Word = 'F_word'
F_Sentiment = 'F_sentiment'


# Features selection
F_VarianceThreshold_All = 'f_varianceThreshold_all'
F_SelectKBest_All = 'f_selectKBest_All'
F_dict = {
    F_Raw: "Number of posts",
    F_Raw_5: "Number of posts (5)",
    F_Raw_Norm: "Number of posts (Normalized)",
    F_N_Local_Spike: "Number local spike",
    F_D_Spike: 'Spike distance',
    F_Latest_Spike: 'Latest spike',
    F_Airl: 'Avg increase',
    F_Aarl: 'Avg acceleration',
    F_Airl_5: 'Avg increase 5 ',
    F_Aarl_5: 'Avg acceleration 5',
    F_User: 'User followers and follow-ees',
    F_VarianceThreshold_All: 'Variance Threshold FS from all features',
    F_SelectKBest_All: 'Select K Best FS from all features',
    F_Paa: 'Paa of time Btw posts',
    F_Line: 'Avg line',
    F_Word: 'Avg word',
    F_Sentiment: 'Sentiment core'
}


# Method
M_Svm = 'm_svm'
M_DecisionTree = 'm_decisionTree'
M_RandomForest = 'm_randomForest'
M_LogisticRegression = 'm_logisticRegression'
M_NeuralNetwork = 'm_neuralNetwork'
M_AdaBoost = 'm_adaBoost'
M_GradientBoosting = 'm_gradientBoosting'
M_Dict = {
    M_Svm: "SVM",
    M_DecisionTree: "Decision Tree",
    M_RandomForest: "Random Forest",
    M_LogisticRegression: "Logistic Regression",
    M_NeuralNetwork: "Neural Network",
    M_AdaBoost: "Ada Boost",
    M_GradientBoosting: "Gradient Boosting DT"
}

# Evaluation
E_Normal = 'E_normal'
E_AllForOne = 'E_allForOne'

# Pattern
P_MinMax = 'p_min_max'
P_RandomNeg = 'p_random_neg'
P_AllNeg = 'p_all_neg'
P_HotRange = 'p_hot_range'
P_EmergingScore = 'p_emerging_score'
P_Dict = {
    P_MinMax: "Min Max Pattern generation method",
    P_HotRange: "Hot range generation method",
    P_EmergingScore: "Emerging score method"
}


# Time slot config
T_Scaled = 't_scaled'
T_Hours = 't_hours'
T_Dict = {
    T_Scaled: "Scaled",
    T_Hours: "ByHours"
}

# Time slot config

GetOrCreateMode = 'GetOrCreateMode'
UpdateMode = 'UpdateMode'

# ----------------------
Patterns_Default = [P_MinMax, P_HotRange]
TimeSlot_Default = [T_Hours]
GenerateFeatures_Default = [F_Raw, F_Raw_Norm, F_N_Local_Spike, F_D_Spike, F_Latest_Spike, F_User, F_Airl, F_Aarl,
                           F_Paa, F_Line, F_Word, F_Sentiment, F_Raw_5, F_Airl_5, F_Aarl_5]

#LearningFeatures_Default = []
LearningFeatures_Default = [F_Raw, F_Raw_Norm, F_N_Local_Spike, F_D_Spike, F_Latest_Spike, F_User, F_Airl, F_Aarl,
                            F_Paa, F_Line, F_Word, F_Sentiment, F_Raw_5, F_Airl_5, F_Aarl_5]
# LearningFeatures_Default = [F_Raw, F_Raw_Norm, F_User, F_Airl]

# LearningMultiFeatures_Default = []

LearningMultiFeatures_Default = [[F_Raw, F_User],
                                  [F_Raw, F_Airl],
                                  [F_Raw, F_Aarl_5],
                                  [F_Raw, F_Latest_Spike],
                                  [F_Raw, F_User, F_Airl],
                                  [F_Raw, F_Sentiment],
                                  [F_Raw, F_Line],
                                  [F_Raw, F_Word],
                                  [F_Raw, F_Paa],
                                 [F_Raw, F_D_Spike],
                                 [F_Raw, F_N_Local_Spike],

                                 [F_User, F_Raw_Norm],
                                 [F_User, F_Airl],
                                 [F_User, F_Aarl_5],
                                 [F_User, F_Latest_Spike],

                                 [F_User, F_Sentiment],
                                 [F_User, F_Line],
                                 [F_User, F_Word],
                                 [F_User, F_Paa],
                                 [F_User, F_D_Spike],
                                 [F_User, F_N_Local_Spike],
                                 ]

LearningMethods_Default = [M_Svm, M_DecisionTree, M_RandomForest, M_LogisticRegression, M_NeuralNetwork, M_AdaBoost,
                           M_GradientBoosting]
EvaluationMethods_Default = [10]


# ----- Config -------------
# Max min
PatternsProfile_Default = PatternsProfile(Patterns_Default, TimeSlot_Default, [10], [1],
                                          GenerateFeatures_Default)
LearningProfile_Default = LearningProfile(Patterns_Default, TimeSlot_Default, [10], [1],
                                          LearningFeatures_Default,
                                          LearningMethods_Default,
                                          EvaluationMethods_Default,
                                          LearningMultiFeatures_Default)


PrintFormat = " > 10,.3f"
DebugMode = True



