
class PatternsProfile(object):
    def __init__(self, patterns, time_slot_configs, k_params, u_params, generate_features):
        self.patterns_generation = patterns
        self.time_slot_configs = time_slot_configs
        self.k_params = k_params
        self.u_params = u_params
        self.generate_features = generate_features


class LearningProfile(object):
    def __init__(self, patterns, time_slot_configs, k_params, u_params,
                 learning_features, learning_methods, evaluation_methods,
                 learning_multi_features):
        self.patterns = patterns
        self.time_slot_configs = time_slot_configs
        self.k_params = k_params
        self.u_params = u_params

        self.learning_features = learning_features

        self.evaluation_methods = evaluation_methods
        self.learning_methods = learning_methods
        self.learning_multi_features = learning_multi_features





