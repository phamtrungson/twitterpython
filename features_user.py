from config import F_User
from domain_extension import get_tweets_by_time_slot_idx, get_user
from features_extension import *
import math


# User log follower count + log friends count
def generate_f_user(data, feature=F_User):
    for event_id, event in data['events'].iteritems():
        field_p = f_p(feature)
        field_n = f_n(feature)
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_followers_count(event, field_p, i)

        for i in event['n_idx']:
            build_followers_count(event, field_n, i)

        add_generated_feature(data, feature)
    return data


def build_followers_count(event, field, p_idx):
    vector = []
    for time_slot in range(p_idx + 1 - 20, p_idx + 1):
        followers_count = 0
        for tweet in get_tweets_by_time_slot_idx(event, time_slot):
            user = get_user(tweet)
            followers_count += float(math.log1p(user['followers_count']) + math.log1p(user['friends_count']))
        vector.append(followers_count)
    event[field].append(vector)
