from sklearn import svm
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.feature_selection import VarianceThreshold, SelectKBest, f_classif
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_validate
from sklearn import tree
from sklearn.neural_network import MLPClassifier

import parameter
from features_fluctuation import *
from features_user import *
from partern_generation import find_hot_range_by_ewma

from utility import avg_list


def learning_process(profile=LearningProfile_Default, mode = 1):
    for time_slot_config in profile.time_slot_configs:
        for pattern in profile.patterns:
            for u in profile.u_params:
                for k in profile.k_params:
                    parameter.Param_Pattern_Length = k
                    parameter.Param_Pattern_Length_2 = parameter.Param_Pattern_Length / 2
                    parameter.Predict_For = u

                    data_with_patterns_features = read_patterns_features(pattern, time_slot_config, k, u)
                    events = data_with_patterns_features['events']
                    print "Pattern: ", P_Dict[pattern], T_Dict[time_slot_config], "k: ", k, "u: ", u
                    print "Positive: ", sum([len(event['p_idx']) for event_id, event in events.iteritems()])
                    print "Negative: ", sum([len(event['n_idx']) for event_id, event in events.iteritems()])
                    output_evaluation_header()

                    if mode == 1:
                        learn_evaluate_on_features(data_with_patterns_features, profile)
                    else:
                        feature_selection_process(data_with_patterns_features, profile)


def learn_evaluate_on_features(data_with_patterns_features, profile):
    learning_features_groups = [[f] for f in profile.learning_features]
    learning_features_groups.extend(profile.learning_multi_features)
    index = 0
    for learning_features_group in learning_features_groups:
        matrix = combine_features_and_labeling(data_with_patterns_features, learning_features_group)

        if DebugMode == 1:
            write_array_csv(matrix)

        x, y = matrix[:, :-1], matrix[:, -1]

        learn_evaluate(index, learning_features_group, profile, x, y)


def learn_evaluate(index, learning_features_group, profile, x, y):
    for learning_method in profile.learning_methods:
        for evaluation_method in profile.evaluation_methods:
            if learning_method == M_Svm:
                clf = svm.SVC(gamma='scale')
                clf.fit(x, y)

            if learning_method == M_DecisionTree:
                clf = tree.DecisionTreeClassifier()
                clf.fit(x, y)

            if learning_method == M_RandomForest:
                clf = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)
                clf.fit(x, y)

            if learning_method == M_LogisticRegression:
                clf = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial')
                clf.fit(x, y)

            if learning_method == M_NeuralNetwork:
                clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
                clf.fit(x, y)

            if learning_method == M_AdaBoost:
                clf = AdaBoostClassifier(n_estimators=100)
                clf.fit(x, y)

            if learning_method == M_GradientBoosting:
                clf = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0)
                clf.fit(x, y)

            if evaluation_method != E_Normal and evaluation_method != E_AllForOne:
                result = cross_validate(clf, x, y, scoring=['precision', 'recall', 'accuracy', 'f1'],
                                        cv=evaluation_method)
                index += 1
                output_evaluation(evaluation_method, learning_features_group, learning_method, result, index)


def feature_selection_process(data_with_patterns_features, profile=LearningProfile_Default):
    # learning_features_groups = [[f] for f in profile.learning_features]
    learning_features_groups = [profile.learning_features]
    index = 0
    for learning_features_group in learning_features_groups:

        # feature selection
        matrix = combine_features_and_labeling(data_with_patterns_features, learning_features_group)
        x, y = matrix[:, :-1], matrix[:, -1]

        selector = VarianceThreshold(threshold=(.6 * (1 - .6)))
        selected_x = selector.fit_transform(x)
        print learning_features_group[0]
        print "Select ", selected_x.shape[1], " / ", x.shape[1]
        print " ".join(format(i, PrintFormat) for i in selector.variances_)

        learn_evaluate(index, [F_VarianceThreshold_All], profile, selected_x, y)

        # select K Best
        selector = SelectKBest(f_classif, k=30)
        selected_x = selector.fit_transform(x, y)
        print learning_features_group[0]
        print "Select ", selected_x.shape[1], " / ", x.shape[1]
        print " ".join(format(i, PrintFormat) for i in selector.scores_)

        learn_evaluate(index, [F_SelectKBest_All], profile, selected_x, y)


def labeling_a_feature(data, feature=F_Raw):
    events = data['events']
    rows = []
    for event_id, event in events.iteritems():
        rows.extend([i + [1] for i in event[f_p(feature)]])
        rows.extend([i + [0] for i in event[f_n(feature)]])
    return np.array(rows)


def combine_features_and_labeling(data, features=[F_Raw]):
    if any(feature not in data['generated_features'] for feature in features):
        raise ValueError("Learning feature is not in generated features")
    events = data['events']
    rows = []
    for event_id, event in events.iteritems():
        for i in range(0, len(event['p_idx'])):
            pattern = []
            for feature in features:
                pattern.extend(event[f_p(feature)][i])
            pattern = pattern + [1]
            rows.append(pattern)
    for event_id, event in events.iteritems():
        for i in range(0, len(event['n_idx'])):
            pattern = []
            for feature in features:
                pattern = pattern + event[f_n(feature)][i]
            pattern = pattern + [0]
            rows.append(pattern)
    return np.array(rows)


def output_evaluation_header():
    print "No,Features,Learning_Method,Evaluation_Method,Precision,Recall,F1,Accuracy,"


def output_evaluation(evaluation_method, learning_features_group, learning_method, result, index):
    print_items = [str(index),
                   "|".join([F_dict[feature] for feature in learning_features_group]),
                   M_Dict[learning_method],
                   "CV" + str(evaluation_method),
                   format(avg_list(result['test_precision']), PrintFormat),
                   format(avg_list(result['test_recall']), PrintFormat),
                   format(avg_list(result['test_f1']), PrintFormat),
                   format(avg_list(result['test_accuracy']), PrintFormat), " "]
    print ",".join(print_items)


def output_old_evaluation(evaluation_method, learning_features_group, learning_method, result):
    print "-------------------------------------------"
    print "Features:" + "|".join([F_dict[feature] for feature in learning_features_group]) + ","
    print "Learning_Method: " + M_Dict[learning_method]
    print "Cross Validation: " + str(evaluation_method) + " fold(s) (Accuracy, Precision, Recall, F1)"
    print_result_array(result['test_accuracy'])
    print_result_array(result['test_precision'])
    print_result_array(result['test_recall'])
    print_result_array(result['test_f1'])


def print_result_array(result_array):
    avg_array = sum(result_array) / len(result_array)
    print format(avg_array, PrintFormat) + "     [", " ".join(format(i, PrintFormat) for i in result_array), "]"
