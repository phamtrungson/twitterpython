

def add_generated_feature(data, feature):
    if feature not in data['generated_features']:
        data['generated_features'].append(feature)


def f_p(f): return f + '_p'


def f_n(f): return f + '_n'

