import json
import os
import re
import shutil
from datetime import datetime, date
from datetime import timedelta
import dateutil.parser
from collections import defaultdict

from config import P_MinMax
from parameter import Minimum_Tweet


def get_data():
    folder_path = "data_event_2012_crawl_1"
    name_pattern = 'Topic-(\w*)-Tweets-(.+)'
    data = defaultdict()
    data['generated_features'] = []
    data['events'] = defaultdict()
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)
        event_id = re.split(name_pattern, file_name)[1]
        event = data['events'][event_id] = {
            'event_id': event_id,
            'num_tweets': 0,
            'max_date': datetime(1999, 1, 1),
            'min_date': datetime(2099, 1, 1),
            'list': [],
        }
        with open(file_path) as event_file:
            for line in event_file:
                obj = json.loads(line)
                obj['CreatedAt'] = dateutil.parser.parse(obj['CreatedAt'])
                event['list'].append(obj)

        event['num_tweets'] = len(event['list'])
        if event['num_tweets'] > 0:
            event['max_date'] = max(event['list'], key=lambda x: x['CreatedAt'])['CreatedAt']
            event['min_date'] = min(event['list'], key=lambda x: x['CreatedAt'])['CreatedAt']
            event['date_range'] = event['max_date'] - event['min_date']
    data['events'] = {k: v for k, v in data['events'].iteritems() if v['num_tweets'] >= Minimum_Tweet}
    return data


def get_data_full(date_field='CreatedAt'):
    folder_path = "data_event_2012_crawl_full/"
    data = defaultdict()
    data['generated_features'] = []
    data['events'] = defaultdict()
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)
        event_id = file_name.split('.')[0]
        event = data['events'][event_id] = {
            'event_id': event_id,
            'num_tweets': 0,
            'max_date': datetime(1999, 1, 1),
            'min_date': datetime(2099, 1, 1),
            'list': [],
        }
        with open(file_path) as event_file:
            for line in event_file:
                tweet_list = json.loads(line)
                for tweet in tweet_list:
                    tweet[date_field] = dateutil.parser.parse(tweet[date_field])
                    event['list'].append(tweet)

        event['num_tweets'] = len(event['list'])
        if event['num_tweets'] > 0:
            event['max_date'] = max(event['list'], key=lambda x: x[date_field])[date_field]
            event['min_date'] = min(event['list'], key=lambda x: x[date_field])[date_field]
            event['date_range'] = event['max_date'] - event['min_date']
    data['events'] = {k: v for k, v in data['events'].iteritems() if v['num_tweets'] >= Minimum_Tweet}
    return data


def write_event_info(events):
    file_path = "output/event_info.txt"
    with open(file_path, 'wb') as write_file:
        avg_date_range = sum([event['date_range'].total_seconds() for event in events.itervalues()]) / len(events)
        write_file.write(max_string(events, 'date_range'))
        write_file.write(min_string(events, 'date_range'))
        write_file.write('avg_date_range: {}\n'.format(timedelta(seconds=avg_date_range)))

        write_file.write(max_string(events, 'num_tweets'))
        write_file.write(min_string(events, 'num_tweets'))
        write_file.write(avg_string(events, 'num_tweets'))
        write_file.write('\n')
        for event_id, event in events.iteritems():
            write_file.write('event_id : {}\n'.format(event_id))
            for key in event:
                if key != 'list':
                    write_file.write('{} : {}\n'.format(key, event[key]))
            write_file.write('\n')


def max_string(events, field):
    max_field = max(events.itervalues(), key=lambda x: x[field])[field]
    return 'max_{}: {}\n'.format(field, max_field)


def min_string(events, field):
    min_field = min(events.itervalues(), key=lambda x: x[field])[field]
    return 'min_{}: {}\n'.format(field, min_field)


def avg_string(events, field):
    avg_field = sum([event[field] for event in events.itervalues()]) / len(events)
    return 'avg_{}: {}\n'.format(field, avg_field)


def get_pattern_info(events):
    p_events = []
    n_events = []
    for event_id, event in events.iteritems():
        if len(event['p_idx']) > 0:
            p_events.append(event_id)
        if len(event['n_idx']) > 0:
            n_events.append(event_id)
    return p_events, n_events


def write_array_txt(matrix):
    file_path = "output/temp_array.txt"
    with open(file_path, 'wb') as write_file:
        for row in matrix:
            write_file.write(" ".join([str(n) for n in row])+"\n")


def write_array_csv(matrix):
    file_path = "output/temp_array.csv"
    with open(file_path, 'wb') as write_file:
        for row in matrix:
            write_file.write(",".join([str(n) for n in row])+"\n")


def write_patterns_features(data, name, time_slot_config, k, u):
    file_name_base = "output/patterns_features_" + name + "_" + time_slot_config + "_k" + str(k) + "_u" + str(u)
    file_path = file_name_base + ".txt"
    with open(file_path, 'wb') as write_file:
        write_file.write(str(len(data['generated_features']))+"\n")
        write_file.writelines([f + "\n" for f in data['generated_features']])
        for event_id, event in data['events'].iteritems():
            if "list" in event:
                del event['list']
            write_file.write(json.dumps(event, default=datetime_handler)+"\n")
    shutil.copyfile(file_path, file_name_base + "_" + datetime.now().strftime("%Y_%m_%d_%H_%M_%S")+".txt")


def read_patterns_features(name, time_slot_config, k, u):
    file_path = "output/patterns_features_"+name + "_" + time_slot_config + "_k" + str(k) + "_u" + str(u) + ".txt"
    data = defaultdict()
    with open(file_path) as read_file:
        generated_feature_len = read_file.readline()
        data['generated_features'] = [read_file.readline().rstrip("\n") for _ in range(int(generated_feature_len))]
        data['events'] = defaultdict()
        for line in read_file:
            obj = json.loads(line)
            data['events'][obj['event_id']] = obj
    return data


def datetime_handler(x):
    if isinstance(x, datetime) or isinstance(x, date):
        return x.isoformat()
    if isinstance(x, timedelta):
        return (datetime.min + x).time().isoformat()
    print x
    raise TypeError("Unknown type")
