import numpy as np

import parameter
from domain_extension import start_index_f_raw
from features_extension import *
from data_access import *
from config import *
from scipy.signal import argrelextrema


# n local spike
from parameter import Param_F_Spike_Order


def generate_f_fluctuation_n_local_spike(data, feature=F_N_Local_Spike):
    for event_id, event in data['events'].iteritems():
        time_series = event['time_series']
        field_p = f_p(feature)
        field_n = f_n(feature)
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_n_spike(event, field_p, i, time_series)

        for i in event['n_idx']:
            build_n_spike(event, field_n, i, time_series)

        add_generated_feature(data, feature)
    return data


def build_n_spike(event, field, idx, time_series):
    spikes = get_spikes(time_series, idx)
    event[field].append([len(spikes)])


# Adll feature
def generate_f_fluctuation_d_spike(data, feature=F_D_Spike):
    for event_id, event in data['events'].iteritems():
        time_series = event['time_series']
        field_p = f_p(feature)
        field_n = f_n(feature)
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_d_spike(event, field_p, i, time_series)

        for i in event['n_idx']:
            build_d_spike(event, field_n, i, time_series)

        add_generated_feature(data, feature)
    return data


def build_d_spike(event, field, idx, time_series):
    spikes = get_spikes(time_series, idx)
    avg = 100
    if len(spikes) > 1:
        d = [spikes[n] - spikes[n - 1] for n in range(1, len(spikes))]
        avg = sum(d) / float(len(d));
    event[field].append([avg])


def generate_f_fluctuation_latest_spike(data, feature=F_Latest_Spike):
    for event_id, event in data['events'].iteritems():
        time_series = event['time_series']
        field_p = f_p(feature)
        field_n = f_n(feature)
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_latest_spike(event, field_p, i, time_series)

        for i in event['n_idx']:
            build_latest_spike(event, field_n, i, time_series)

        add_generated_feature(data, feature)
    return data


def build_latest_spike(event, field, idx, time_series):
    spikes = get_spikes(time_series, idx)
    event[field].append([parameter.Param_Pattern_Length - spikes[-1] if len(spikes) > 0 else 40])


def get_spikes(time_series, idx):
    return argrelextrema(
        np.array(time_series[start_index_f_raw(idx): idx + 1]), np.greater, order=Param_F_Spike_Order)[0]

