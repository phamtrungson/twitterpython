from tslearn.piecewise import PiecewiseAggregateApproximation
from nltk.sentiment.vader import SentimentIntensityAnalyzer

from config import F_Paa, F_Line, F_Word, F_Sentiment
from domain_extension import get_tweets_by_pattern_idx, get_full_text
from features_extension import f_p, f_n, add_generated_feature
from parameter import Param_F_Ppa
from utility import avg_list


def generate_f_paa(data, feature=F_Paa):
    for event_id, event in data['events'].iteritems():
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_paa(event, f_p(feature), i)

        for i in event['n_idx']:
            build_paa(event, f_n(feature), i)

        add_generated_feature(data, feature)
    return data


def build_paa(event, field, idx):
    list_tweets = get_tweets_by_pattern_idx(event, idx)
    len_list = len(list_tweets)
    if len_list <= Param_F_Ppa:
        event[field].append(range(999, 999+Param_F_Ppa))
        return

    list_tweets.sort(key=lambda x: x['CreatedAt'])
    time_btw_tweets = []
    for i in range(1, len_list):
        tweet = list_tweets[i]
        pre_tweet = list_tweets[i-1]
        time_btw_tweets.append(int(round((tweet['CreatedAt'] - pre_tweet['CreatedAt']).total_seconds())))

    paa = PiecewiseAggregateApproximation(Param_F_Ppa)
    ppa_time_btw_tweets = paa.fit_transform(time_btw_tweets)[0].flatten()
    event[field].append(ppa_time_btw_tweets.tolist())


def generate_f_line(data, feature=F_Line):
    for event_id, event in data['events'].iteritems():
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_f_line(event, f_p(feature), i)

        for i in event['n_idx']:
            build_f_line(event, f_n(feature), i)

        add_generated_feature(data, feature)
    return data


def build_f_line(event, field, idx):
    list_tweets = get_tweets_by_pattern_idx(event, idx)
    lines = [len(get_full_text(tweet).split('.')) for tweet in list_tweets]
    if len(lines) > 0:
        event[field].append([avg_list(lines)])
    else:
        event[field].append([0])


def generate_f_word(data, feature=F_Word):
    for event_id, event in data['events'].iteritems():
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_f_word(event, f_p(feature), i)

        for i in event['n_idx']:
            build_f_word(event, f_n(feature), i)

        add_generated_feature(data, feature)
    return data


def build_f_word(event, field, idx):
    list_tweets = get_tweets_by_pattern_idx(event, idx)
    lines = [len(get_full_text(tweet).split()) for tweet in list_tweets]
    if len(lines) > 0:
        event[field].append([avg_list(lines)])
    else:
        event[field].append([0])


def generate_f_sentiment(data, feature=F_Sentiment):
    for event_id, event in data['events'].iteritems():
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_f_sentiment(event, f_p(feature), i)

        for i in event['n_idx']:
            build_f_sentiment(event, f_n(feature), i)

        add_generated_feature(data, feature)
    return data


def build_f_sentiment(event, field, idx):
    list_tweets = get_tweets_by_pattern_idx(event, idx)
    analyser = SentimentIntensityAnalyzer()
    scores = [analyser.polarity_scores(get_full_text(tweet)) for tweet in list_tweets]
    if len(scores) > 0:
        negs = [score['neg'] for score in scores]
        neus = [score['neu'] for score in scores]
        poss = [score['pos'] for score in scores]
        compounds = [score['compound'] for score in scores]
        event[field].append([avg_list(negs), avg_list(neus), avg_list(poss), avg_list(compounds)])
    else:
        event[field].append([0, 0, 0, 0])