import parameter
from config import F_Airl, F_Airl_5, F_Aarl_5, F_Aarl
from domain_extension import start_index_f_timeseries_each_l, start_index_f_timeseries_l
from features_extension import *



# Timeseries avg one step inc : 1-len vector
# avg on step inc in lastest l time window (time point)
# AIRL
def generate_f_timeseries_airl(data, feature=F_Airl_5):

    for event_id, event in data['events'].iteritems():
        time_series = event['time_series']
        field_p = f_p(feature)
        field_n = f_n(feature)
        event[f_p(feature)] = []
        event[f_n(feature)] = []
        for i in event['p_idx']:
            build_raw(event, field_p, i, time_series, feature)

        for i in event['n_idx']:
            build_raw(event, field_n, i, time_series, feature)

        add_generated_feature(data, feature)
    return data


def build_raw(event, field, idx, time_series, feature):
    param_l = parameter.Param_Pattern_Length if feature == F_Airl or feature == F_Aarl else int(parameter.Param_Pattern_Length)
    if feature == F_Airl_5 or feature == F_Airl:
        vector = [avg_1_step_inc(
            time_series[start_index_f_timeseries_each_l(i, param_l): i + 1]) for i in range(
                start_index_f_timeseries_l(idx, param_l), idx+1)]

    if feature == F_Aarl_5 or feature == F_Aarl:
        vector = [avg_1_step_acceleration(
            time_series[start_index_f_timeseries_each_l(i, param_l): i + 1]) for i in range(
                start_index_f_timeseries_l(idx, param_l), idx + 1)]

    event[field].append(vector)


def avg_1_step_inc(time_series):
    _1_step_inc = [time_series[i+1]-time_series[i] for i in range(0, len(time_series)-1)]
    return sum(_1_step_inc) / float(len(_1_step_inc))


def avg_1_step_acceleration(time_series):
    _1_step_inc = [time_series[i+1]-time_series[i] for i in range(0, len(time_series)-1)]
    _1_step_acceleration = [_1_step_inc[i + 1] - _1_step_inc[i] for i in range(0, len(_1_step_inc) - 1)]
    return sum(_1_step_acceleration) / float(len(_1_step_acceleration))

