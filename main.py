from math import sqrt

from sklearn.model_selection import cross_val_score
from sklearn.datasets import load_iris
from sklearn import tree

import time
from config import *
from data_access import get_data_full
from learning_extension import learning_process, feature_selection_process
from partern_generation import generate_patterns_features, ewma_list, ewma_var, find_hot_range_by_ewma, \
    map_to_time_series

start = time.time()

# find_hot_range_by_ewma(map_to_time_series(get_data_full()))
generate_patterns_features()
learning_process(mode=1)
# feature_selection_process()

# iris = load_iris()
# clf = tree.DecisionTreeClassifier()
# clf = clf.fit(iris.data, iris.target)

# print cross_val_score(clf, iris.data, iris.target, scoring='f1_macro', cv=10)


end = time.time()
print 'finished:', end - start

